﻿namespace TransactionApp.DAL.Enums
{
    public enum TransactionStatus
    {
        A,
        R,
        D
    }
}
