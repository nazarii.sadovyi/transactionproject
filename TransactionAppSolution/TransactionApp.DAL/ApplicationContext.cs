﻿using Microsoft.EntityFrameworkCore;
using System;
using TransactionApp.DAL.DbEntities;

namespace TransactionApp.DAL
{
    public class ApplicationContext: DbContext
    {
        public DbSet<TransactionData> TransactionDatas { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated(); 
        }
    }
}
