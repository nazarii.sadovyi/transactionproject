﻿using System.Linq;
using TransactionApp.DAL.DbEntities;
using TransactionApp.DAL.Filters;

namespace TransactionApp.DAL.Repositories
{
    public interface ITransactionDataRepository
    {
        IQueryable<TransactionData> GetAll();
        void Add(TransactionData transaction);
        IQueryable<TransactionData> Filter(TransactionFilter filter);
    }
    
    public class TransactionDataRepository: ITransactionDataRepository
    {
        private readonly ApplicationContext _applicationContext;
        public TransactionDataRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(TransactionData transaction)
        {
            _applicationContext.TransactionDatas.Add(transaction);
            _applicationContext.SaveChanges();
        }

        public IQueryable<TransactionData> GetAll()
        {
            return _applicationContext.TransactionDatas.AsQueryable();
        }

        public IQueryable<TransactionData> Filter(TransactionFilter filter)
        {
            var transactionQueryBuilder = _applicationContext.TransactionDatas.AsQueryable();

            if (!string.IsNullOrEmpty(filter.Currency))
            {
                transactionQueryBuilder = transactionQueryBuilder.Where(x => x.CurrencyCode == filter.Currency);
            }

            if (filter.Status.HasValue)
            {
                transactionQueryBuilder = transactionQueryBuilder.Where(x => x.Status == filter.Status.Value);
            }

            if (filter.MinDate.HasValue)
            {
                transactionQueryBuilder = transactionQueryBuilder.Where(x => x.Date >= filter.MinDate.Value);
            }

            if (filter.MaxDate.HasValue)
            {
                transactionQueryBuilder = transactionQueryBuilder.Where(x => x.Date <= filter.MaxDate.Value);
            }

            return transactionQueryBuilder;
        }
    }
}
