﻿using System;
using TransactionApp.Common.Domain;
using TransactionApp.Common.Enums;
using TransactionApp.DAL.DbEntities;
using TransactionApp.DAL.Enums;

namespace TransactionApp.DAL.Helpers
{
    public static class TransactionDataHelper
    {
        public static TransactionStatus ToDbTransactionStatus(this CommonTransactionStatus state)
        {
            switch (state)
            {
                case CommonTransactionStatus.A:
                    return TransactionStatus.A;
                case CommonTransactionStatus.R:
                    return TransactionStatus.R;
                case CommonTransactionStatus.D:
                    return TransactionStatus.D;
                default:
                    throw new NotSupportedException(state.ToString());
            }
        }

        public static CommonTransactionStatus ToDomainStatus(this TransactionStatus state)
        {
            switch (state)
            {
                case TransactionStatus.A:
                    return CommonTransactionStatus.A;
                case TransactionStatus.R:
                    return CommonTransactionStatus.R;
                case TransactionStatus.D:
                    return CommonTransactionStatus.D;
                default:
                    throw new NotSupportedException(state.ToString());
            }
        }

        public static TransactionData ToDbTransactionData(this Transaction domainTransaction)
        {
            return new TransactionData()
            {
                Id = domainTransaction.Id,
                Amount = domainTransaction.Amount,
                CurrencyCode = domainTransaction.CurrencyCode,
                Date = domainTransaction.Date,
                Status = domainTransaction.State.ToDbTransactionStatus(),
            };
        }

        public static Transaction ToDomain(this TransactionData transaction)
        {
            return new Transaction()
            {
                Id = transaction.Id,
                Amount = transaction.Amount,
                CurrencyCode = transaction.CurrencyCode,
                Date = transaction.Date,
                State = transaction.Status.ToDomainStatus(),
            };
        }
    }
}
