﻿using System;
using System.ComponentModel.DataAnnotations;
using TransactionApp.DAL.Enums;

namespace TransactionApp.DAL.DbEntities
{
    public class TransactionData
    {
        [Key]
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }
        public TransactionStatus Status { get; set; }
    }
}
