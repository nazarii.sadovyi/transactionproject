﻿using System;
using TransactionApp.DAL.Enums;

namespace TransactionApp.DAL.Filters
{
    public class TransactionFilter
    {
        public string Currency { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public TransactionStatus? Status { get; set; }
    }
}
