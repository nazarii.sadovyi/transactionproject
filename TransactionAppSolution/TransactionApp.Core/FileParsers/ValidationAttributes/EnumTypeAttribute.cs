﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransactionApp.Core.FileParsers.ValidationAttributes
{
    public class EnumTypeAttribute: ValidationAttribute
    {
        private readonly Type _enumtype;
        public EnumTypeAttribute(Type enumtype)
        {
            if (!enumtype.IsEnum)
            {
                throw new ArgumentException("Input type is not enum");
            }
            _enumtype = enumtype;
        }

        public override bool IsValid(object value)
        {
            if (value is string _value)
            {
                var parseResult = Enum.TryParse(_enumtype, _value, out object _);
                if (!parseResult)
                {
                    ErrorMessage = "Can't convert field {0} value to expected enum";
                }
                return parseResult;
            }
            return false;
        }
    }
}
