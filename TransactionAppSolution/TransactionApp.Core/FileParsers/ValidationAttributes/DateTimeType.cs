﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransactionApp.Core.FileParsers.ValidationAttributes
{
    public class DateTimeType : ValidationAttribute
    {
        private readonly string _format;
        public DateTimeType(string format)
        {
            _format = format;
        }
        public override bool IsValid(object value)
        {
            if (value is string _value)
            {
                var result = DateTime.TryParse(_value, out DateTime parsedValue);

                if (result == false)
                {
                    ErrorMessage = "Can't parse field {0} value to DateTime";
                    return false;
                }

                var formatedValue = parsedValue.ToString(_format);
                var isSame = formatedValue == _value;

                if (!isSame)
                {
                    ErrorMessage = "Field {0} value is not in expected format";
                }

                return isSame;
            }

            return false;
        }
    }
}
