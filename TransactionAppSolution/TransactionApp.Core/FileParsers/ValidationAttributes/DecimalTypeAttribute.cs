﻿using System.ComponentModel.DataAnnotations;

namespace TransactionApp.Core.FileParsers.ValidationAttributes
{
    public class DecimalTypeAttribute: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is string _value)
            {
                var parseResult = decimal.TryParse(_value, out _);
                if (!parseResult)
                {
                    ErrorMessage = "Can't convert field {0} value to decimal";
                }
                return parseResult;
            }
            return false;
        }
    }
}
