﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace TransactionApp.Core.FileParsers.ValidationAttributes
{
    public class CurrencySymbolAttribute : ValidationAttribute
    {
        private static readonly IEnumerable<string> Currencies;
        static CurrencySymbolAttribute()
        {
            Currencies = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                .Select(ci => ci.LCID).Distinct()
                .Select(id => new RegionInfo(id))
                .GroupBy(r => r.ISOCurrencySymbol)
                .Select(g => g.First())
                .Select(r => r.ISOCurrencySymbol);
        }
        public override bool IsValid(object value)
        {
            if (value is string _value)
            {
                var result = CurrencySymbolAttribute.Currencies.Contains(_value);
                if (!result)
                {
                    ErrorMessage = "Field {0} is not in Currency Symbol format";
                }
                return result;
            }
            
            return true;
        }
    }
}
