﻿using System;
using TransactionApp.Common.Enums;
using TransactionApp.Core.FileParsers.ValidationAttributes;
using System.ComponentModel.DataAnnotations;
using TransactionApp.Common.Domain;
using TransactionApp.Common.Helpers;

namespace TransactionApp.Core.FileParsers.InitModels
{
    public class XMLTransactionInitSimpled : BaseInputModel<Transaction>
    {
        [MaxLength(50)]
        public string Id { get; set; }

        [DateTimeType("yyyy-MM-ddTHH:mm:ss")]
        public string TransactionDate { get; set; }

        [DecimalType]
        public string Amount { get; set; }

        [CurrencySymbol]
        public string CurrencyCode { get; set; }

        [EnumType(typeof(XMLTransactionState))]
        public string Status { get; set; }

        public override Transaction ToModel()
        {
            return new Transaction()
            {
                Id = Id,
                Date = Convert.ToDateTime(TransactionDate),
                Amount = Convert.ToDecimal(Amount),
                CurrencyCode = CurrencyCode,
                State = Enum.Parse<XMLTransactionState>(Status).ToCommonTransactionState()
            };
        }
    }
}
