﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace TransactionApp.Core.FileParsers.InitModels
{
    [XmlRoot("Transactions")]
    public class XMLTransactionInit
    {
        [XmlElement("Transaction")]
        public List<TransactionInit> Transactions { get; set; }
    }

    public class TransactionInit
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("TransactionDate")]
        public string TransactionDate { get; set; }

        [XmlElement("Status")]
        public string Status { get; set; }

        [XmlElement("PaymentDetails")]
        public PaymentDetailsInit PaymentDetails { get; set; }

        public XMLTransactionInitSimpled ToSimpled()
        {
            return new XMLTransactionInitSimpled()
            {
                Id = Id,
                TransactionDate = TransactionDate,
                Amount = PaymentDetails.Amount,
                CurrencyCode = PaymentDetails.CurrencyCode,
                Status = Status
            };
        }
    }

    public class PaymentDetailsInit
    {
        [XmlElement("Amount")]
        public string Amount { get; set; }

        [XmlElement("CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}
