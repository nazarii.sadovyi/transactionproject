﻿using FileHelpers;

namespace TransactionApp.Core.FileParsers.InitModels
{
    [DelimitedRecord(",")]
    public class CSVTransactionInit
    {
        [FieldTrim(TrimMode.Both, ' ')]
        public string Id { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        public string Amount { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        public string CurrencyCode { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        public string TransactionDate { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        public string Status { get; set; }


        public CSVTransactionInitSimpled ToSimpled()
        {
            return new CSVTransactionInitSimpled()
            {
                Id = Id,
                TransactionDate = TransactionDate,
                Amount = Amount,
                CurrencyCode = CurrencyCode,
                Status = Status
            };
        }
    }
}
