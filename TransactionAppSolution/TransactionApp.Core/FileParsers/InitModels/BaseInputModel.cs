﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace TransactionApp.Core.FileParsers.InitModels
{
    public abstract class BaseInputModel<T>
    {
        public bool IsValid 
        {
            get
            {
                return string.IsNullOrEmpty(ValidationError);
            }
        }

        public string ValidationError;

        public void Validate()
        {
            foreach (var prop in GetType().GetProperties())
            {
                var value = prop.GetValue(this);

                if (value == null)
                {
                    ValidationError = $"Field {prop.Name} value not exist or is empty";
                    break;
                }

                var validationAttributes = prop.GetCustomAttributes<ValidationAttribute>();
                foreach (var validationAttribute in validationAttributes)
                {
                    if (!validationAttribute.IsValid(value))
                    {
                        ValidationError = validationAttribute.FormatErrorMessage(prop.Name);
                        break;
                    }
                }

                if (!IsValid)
                {
                    break;
                }
            }
        }

        public abstract T ToModel();
    }
}
