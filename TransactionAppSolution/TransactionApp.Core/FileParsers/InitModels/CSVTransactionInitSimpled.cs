﻿using System;
using TransactionApp.Common.Enums;
using TransactionApp.Core.FileParsers.ValidationAttributes;
using System.ComponentModel.DataAnnotations;
using TransactionApp.Common.Domain;
using TransactionApp.Common.Helpers;

namespace TransactionApp.Core.FileParsers.InitModels
{
    public class CSVTransactionInitSimpled : BaseInputModel<Transaction>
    {
        [MaxLength(50)]
        public string Id { get; set; }

        [DecimalType]
        public string Amount { get; set; }

        [CurrencySymbol]
        public string CurrencyCode { get; set; }

        [DateTimeType("dd'/'MM'/'yyyy HH:mm:ss")]
        public string TransactionDate { get; set; }

        [EnumType(typeof(CSVTransactionState))]
        public string Status { get; set; }

        public override Transaction ToModel()
        {
            return new Transaction()
            {
                Id = Id,
                Date = Convert.ToDateTime(TransactionDate),
                Amount = Convert.ToDecimal(Amount),
                CurrencyCode = CurrencyCode,
                State = Enum.Parse<CSVTransactionState>(Status).ToCommonTransactionState()
            };
        }
    }
}
