﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TransactionApp.Common.Domain;
using TransactionApp.Core.FileParsers.Entities;
using TransactionApp.Core.FileParsers.InitModels;

namespace TransactionApp.Core.FileParsers
{
    public class XMLFileParser: IFileTransactionParser
    {
        public FileParseResult<Transaction> Parse(FileStream fileStream)
        {
            var result = new FileParseResult<Transaction>();

            XmlSerializer serializer = new XmlSerializer(typeof(XMLTransactionInit));
            XMLTransactionInit xmlResult = (XMLTransactionInit)serializer.Deserialize(fileStream);

            var simpledTransactions = xmlResult.Transactions.Select(x => x.ToSimpled());

            for (int i = 0; i < simpledTransactions.Count(); i++)
            {
                var simpledTransaction = simpledTransactions.ElementAt(i);

                simpledTransaction.Validate();
                if (simpledTransaction.IsValid)
                {
                    result.CorrectRecords.Add(simpledTransaction.ToModel());
                }
                else
                {
                    result.IncorrectRecords.Add(new KeyValuePair<int, string>(i, simpledTransaction.ValidationError));
                }
            }

            return result;
        }
    }
}
