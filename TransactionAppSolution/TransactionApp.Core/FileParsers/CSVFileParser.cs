﻿using FileHelpers;
using FileHelpers.Events;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TransactionApp.Common.Domain;
using TransactionApp.Core.FileParsers.Entities;
using TransactionApp.Core.FileParsers.InitModels;

namespace TransactionApp.Core.FileParsers
{
    public class CSVFileParser : IFileTransactionParser
    {
        public FileParseResult<Transaction> Parse(FileStream fileStream)
        {
            var result = new FileParseResult<Transaction>();

            var engine = new FileHelperEngine<CSVTransactionInit>();
            engine.BeforeReadRecord += BeforeEvent;
            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            var records = engine.ReadStream(new StreamReader(fileStream)).ToList();

            var simpledRecords = records.Select(x => x.ToSimpled());

            for (int i = 0; i < simpledRecords.Count(); i++)
            {
                var simpledTransaction = simpledRecords.ElementAt(i);

                simpledTransaction.Validate();
                if (simpledTransaction.IsValid)
                {
                    result.CorrectRecords.Add(simpledTransaction.ToModel());
                }
                else
                {
                    result.IncorrectRecords.Add(new KeyValuePair<int, string>(i, simpledTransaction.ValidationError));
                }
            }

            return result;
        }

        private void BeforeEvent(EngineBase engine, BeforeReadEventArgs<CSVTransactionInit> e)
        {
            e.RecordLine = Regex.Replace(e.RecordLine, '"'.ToString(), "");
        }
    }
}
