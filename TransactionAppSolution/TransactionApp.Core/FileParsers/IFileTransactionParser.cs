﻿using System.IO;
using TransactionApp.Common.Domain;
using TransactionApp.Core.FileParsers.Entities;

namespace TransactionApp.Core.FileParsers
{
    public interface IFileTransactionParser
    {
        FileParseResult<Transaction> Parse(FileStream fileStream);
    }
}
