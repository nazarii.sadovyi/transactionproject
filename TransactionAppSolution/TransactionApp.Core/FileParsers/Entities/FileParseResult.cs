﻿using System.Collections.Generic;

namespace TransactionApp.Core.FileParsers.Entities
{
    public class FileParseResult<T>
    {
        public ICollection<T> CorrectRecords { get; set; }

        /// <summary>
        /// Key is number of line and value is error message
        /// </summary>
        public ICollection<KeyValuePair<int, string>> IncorrectRecords { get; set; }

        public FileParseResult()
        {
            CorrectRecords = new List<T>();
            IncorrectRecords = new List<KeyValuePair<int, string>>();
        }
    }
}
