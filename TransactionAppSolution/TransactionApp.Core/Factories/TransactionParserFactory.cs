﻿using System;
using TransactionApp.Common.Enums;
using TransactionApp.Core.FileParsers;

namespace TransactionApp.Core.Factories
{
    public interface ITransactionParserFactory
    {
        IFileTransactionParser GetFileParser(FileToParseKind fileToParseKind);
    }
    public class TransactionParserFactory : ITransactionParserFactory
    {
        public IFileTransactionParser GetFileParser(FileToParseKind fileToParseKind)
        {
            switch (fileToParseKind)
            {
                case FileToParseKind.CSV:
                    return new CSVFileParser();
                case FileToParseKind.XML:
                    return new XMLFileParser();
                default:
                    throw new NotSupportedException(fileToParseKind.ToString());
            }
        }
    }
}
