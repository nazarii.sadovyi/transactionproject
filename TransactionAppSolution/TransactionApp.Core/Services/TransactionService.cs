﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TransactionApp.Common.Domain;
using TransactionApp.Common.Entities;
using TransactionApp.Common.Enums;
using TransactionApp.Common.Exceptions;
using TransactionApp.Core.Factories;
using TransactionApp.DAL.Repositories;
using TransactionApp.DAL.Helpers;
using TransactionApp.DAL.Filters;
using TransactionApp.DAL.Enums;

namespace TransactionApp.Core.Services
{
    public interface ITransactionService
    {
        TransactionParseResult ImportTransactionFromFile(string filePath);
        IEnumerable<Transaction> GetAll();
        IEnumerable<Transaction> Filter(TransactionFilterParameter filterParameter);
    }
    public class TransactionService: ITransactionService
    {
        private readonly ITransactionParserFactory _transactionParserFactory;
        private readonly ITransactionDataRepository _transactionDataRepository;
        public TransactionService(ITransactionParserFactory transactionParserFactory, 
            ITransactionDataRepository transactionDataRepository)
        {
            _transactionParserFactory = transactionParserFactory;
            _transactionDataRepository = transactionDataRepository;
        }

        public TransactionParseResult ImportTransactionFromFile(string filePath)
        {
            TransactionParseResult transactionParseResult = new TransactionParseResult();

            var extension = Path.GetExtension(filePath);
            FileToParseKind fileToParseKind;
            switch (extension)
            {
                case ".xml":
                    fileToParseKind = FileToParseKind.XML;
                    break;
                case ".csv":
                    fileToParseKind = FileToParseKind.CSV;
                    break;
                default:
                    throw new NotSuportedFileExtension(extension);
            }

            var correctTransactions = new List<Transaction>();

            using (FileStream fstream = new FileStream(filePath, FileMode.Open))
            {
                var fileTransactionParser = _transactionParserFactory.GetFileParser(fileToParseKind);
                var result = fileTransactionParser.Parse(fstream);

                transactionParseResult = new TransactionParseResult()
                {
                    IncorrectRecords = result.IncorrectRecords
                };

                correctTransactions.AddRange(result.CorrectRecords);
            };

            var currectTransactionIds = _transactionDataRepository.GetAll().Select(x => x.Id);

            foreach (var transaction in correctTransactions)
            {
                if (currectTransactionIds.Contains(transaction.Id))
                {
                    transactionParseResult.IncorrectIds.Add(transaction.Id);
                }
                else
                {
                    _transactionDataRepository.Add(transaction.ToDbTransactionData());
                    transactionParseResult.CorrectRecords.Add(transaction);
                }
            }

            return transactionParseResult;
        }

        public IEnumerable<Transaction> GetAll()
        {
            return _transactionDataRepository
                .GetAll()
                .Select(x => x.ToDomain())
                .ToList();
        }

        public IEnumerable<Transaction> Filter (TransactionFilterParameter filterParameter)
        {
            var filteredTransactions = _transactionDataRepository.Filter(new TransactionFilter()
            {
                Currency = filterParameter.Currency,
                MaxDate = filterParameter.MaxDate,
                MinDate = filterParameter.MinDate,
                Status = filterParameter.Status.HasValue ?
                    filterParameter.Status.Value.ToDbTransactionStatus() : (TransactionStatus?)null,
            });

            return filteredTransactions.Select(x => x.ToDomain());
        }
    }
}
