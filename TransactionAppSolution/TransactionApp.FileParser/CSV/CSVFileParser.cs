﻿using FileHelpers;
using FileHelpers.Events;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using TransactionApp.Common.Domain;
using TransactionApp.FileParser.Attributes;
using TransactionApp.FileParser.CSV.Mapping;
using TransactionApp.FileParser.Entities;

namespace TransactionApp.FileParser.CSV
{
    internal class CSVFileParser : IFileParser
    {
        public CSVFileParser()
        {
            
        }

        public FileParseResult<T> Parse<T>(string filePath, T model) where T: new()
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    Regex lineRegex= new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] values = lineRegex.Split(line);

                    foreach (var prop in typeof(T).GetProperties())
                    {
                        var fieldNumberAttribute = prop.GetCustomAttribute<FieldNumberAttribute>();

                        if (fieldNumberAttribute == null)
                        {
                            continue;
                        }

                        var value = values.ElementAtOrDefault(fieldNumberAttribute.Number);

                        if (value == default)
                        {
                            // to do 
                        }

                        foreach (var validationAttribute in prop.GetCustomAttributes<BasePropValidationAttribute>())
                        {
                            var isValid = validationAttribute.IsValid(value);

                            if (isValid)
                            {
                                continue;
                            }


                        }
                    }

                }
            }

            var result = new FileParseResult<Transaction>();

            var engine = new FileHelperEngine<TransactionModel>();
            engine.BeforeReadRecord += BeforeEvent;
            engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            var records = engine.ReadFile(filePath);

            return null;
        }

        private void BeforeEvent(EngineBase engine, BeforeReadEventArgs<TransactionModel> e)
        {
            e.RecordLine = Regex.Replace(e.RecordLine, '"'.ToString(), "");
        }
    }
}
