﻿using FileHelpers;
using System;
using TransactionApp.Common.Enums;
using TransactionApp.FileParser.CSV.Converters;

namespace TransactionApp.FileParser.CSV.Mapping
{
    [DelimitedRecord(",")]
    public class TransactionModel
    {
        [FieldTrim(TrimMode.Both, ' ')]
        public string Id { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        [FieldConverter(ConverterKind.Decimal, ".")]
        public decimal Amount { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        [FieldConverter(typeof(ISOCurrencyConverter))]
        public string CurrencyCode { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy HH:mm:ss")]
        public DateTime Date { get; set; }

        [FieldTrim(TrimMode.Both, ' ')]
        public CSVTransactionState State { get; set; }
    }
}
