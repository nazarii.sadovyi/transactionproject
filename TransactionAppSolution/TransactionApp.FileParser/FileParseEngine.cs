﻿using System.Reflection;
using System.Collections.Generic;
using System;
using TransactionApp.Common.Domain;
using TransactionApp.FileParser.CSV;
using TransactionApp.FileParser.Entities;
using TransactionApp.FileParser.Attributes;

namespace TransactionApp.FileParser
{
    public class FileParseEngine
    {
        public FileParseEngine()
        {

        }

        public FileParseResult<Transaction> Parse(string filePath)
        {
            var t = new CSVFileParser();
            //var tt = t.Parse(filePath);



            return null;
        }

        public void Parse<T>(string filePath, T modelMap) where T: new()
        {
            var parseModelMapAttribute = typeof(T).GetCustomAttribute<ParseModelMapAttribute>();

            if (parseModelMapAttribute == default)
            {
                throw new ArgumentException("Input model not contain ParseModelMapAttribute");
            }

            switch (parseModelMapAttribute.FileType)
            {
                case Enums.FileType.XML:

                    break;
                case Enums.FileType.CSV:

                    break;
                default:
                    throw new NotSupportedException(parseModelMapAttribute.FileType.ToString());
            }

        }
    }
}
