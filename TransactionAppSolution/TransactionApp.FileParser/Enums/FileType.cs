﻿namespace TransactionApp.FileParser.Enums
{
    public enum FileType
    {
        XML,
        CSV
    }
}
