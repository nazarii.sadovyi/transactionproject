﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionApp.FileParser.Entities
{
    public enum PropertyKind
    {
        String,
        Decimal,
        DateTime,
        Enum
    }
}
