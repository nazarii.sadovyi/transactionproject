﻿using System.Collections.Generic;

namespace TransactionApp.FileParser.Entities
{
    public class FileParseResult<T>
    {
        public IEnumerable<T> CorrectRecords { get; set; }

        /// <summary>
        /// Key is number of line and value is error message
        /// </summary>
        public IEnumerable<KeyValuePair<int, string>> IncorrectRecords { get; set; }
    }
}
