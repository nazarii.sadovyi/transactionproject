﻿using System;

namespace TransactionApp.FileParser.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaxLengthAttribute : BasePropValidationAttribute
    {
        private readonly int _length;
        public MaxLengthAttribute(int length)
        {
            _length = length;
            FieldErrorMessage = "Data is too long";
        }

        public override bool IsValid(object value)
        {
            if (value is string _value)
            {
                return _value.Length <= _length;
            }
            return false;
        }
    }
}
