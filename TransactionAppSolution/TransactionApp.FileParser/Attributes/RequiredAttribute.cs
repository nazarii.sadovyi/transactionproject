﻿using System;

namespace TransactionApp.FileParser.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredAttribute: BasePropValidationAttribute
    {
        public RequiredAttribute()
        {
            FieldErrorMessage = "Field is required";
        }
    }
}
