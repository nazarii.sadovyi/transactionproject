﻿using System;
using TransactionApp.FileParser.Enums;

namespace TransactionApp.FileParser.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ParseModelMapAttribute: Attribute
    {
        public readonly FileType FileType;
        public ParseModelMapAttribute(FileType fileType)
        {
            FileType = fileType;
        }
    }
}
