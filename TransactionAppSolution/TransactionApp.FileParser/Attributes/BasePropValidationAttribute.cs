﻿using System.ComponentModel.DataAnnotations;

namespace TransactionApp.FileParser.Attributes
{
    public class BasePropValidationAttribute: ValidationAttribute
    {
        public string FieldErrorMessage { get; set; }
    }
}
