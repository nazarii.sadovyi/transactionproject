﻿using System;

namespace TransactionApp.FileParser.Attributes
{
    public class FieldNumberAttribute: Attribute
    {
        public readonly int Number;
        public FieldNumberAttribute(int number)
        {
            Number = number;
        }
    }
}
