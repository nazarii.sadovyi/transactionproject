﻿using System;
using System.Collections.Generic;
using System.Text;
using TransactionApp.FileParser.Entities;

namespace TransactionApp.FileParser.Attributes
{
    public class PropertyTypeAttribute: Attribute
    {
        public readonly PropertyKind PropertyKind;
        public PropertyTypeAttribute(PropertyKind propertyKind)
        {
            PropertyKind = propertyKind;
        }

        //public bool IsValid(object value)
        //{
        //    try
        //    {
        //        switch (PropertyKind)
        //        {
        //            case PropertyKind.String:
        //                Convert.ToString(value);
        //                return true;
        //            case PropertyKind.Decimal:

        //                break;
        //            case PropertyKind.DateTime:

        //                break;
        //            case PropertyKind.Enum:

        //                break;
        //            default:
        //                throw new NotSupportedException(PropertyKind.ToString());
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }
            
        //}

        //public object ConvertToType(object value)
        //{
            
        //}
    }
}
