﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TransactionApp.Common.Domain;
using TransactionApp.FileParser.Entities;

namespace TransactionApp.FileParser
{
    internal interface IFileParser
    {
        FileParseResult<T> Parse<T>(string filePath, T model) where T: new();
    }
}
