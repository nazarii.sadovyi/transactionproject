﻿using System;

namespace TransactionApp.Common.Exceptions
{
    public class NotSuportedFileExtension: Exception
    {
        public NotSuportedFileExtension(string extension) 
            : base($"File type {extension} is not suported")
        { }
    }
}
