﻿using System;
using TransactionApp.Common.Enums;

namespace TransactionApp.Common.Domain
{
    public class Transaction
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }
        public CommonTransactionStatus State { get; set; }
    }
}
