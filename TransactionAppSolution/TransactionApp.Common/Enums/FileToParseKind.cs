﻿namespace TransactionApp.Common.Enums
{
    public enum FileToParseKind
    {
        CSV,
        XML
    }
}
