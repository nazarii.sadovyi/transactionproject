﻿namespace TransactionApp.Common.Enums
{
    public enum XMLTransactionState
    {
        Approved,
        Rejected,
        Done
    }
}
