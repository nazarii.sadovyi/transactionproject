﻿namespace TransactionApp.Common.Enums
{
    public enum CSVTransactionState
    {
        Approved,
        Failed,
        Finished
    }
}
