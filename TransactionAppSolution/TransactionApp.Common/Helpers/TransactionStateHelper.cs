﻿using System;
using TransactionApp.Common.Enums;

namespace TransactionApp.Common.Helpers
{
    public static class TransactionStateHelper
    {
        public static CommonTransactionStatus ToCommonTransactionState(this XMLTransactionState state)
        {
            switch (state)
            {
                case XMLTransactionState.Approved:
                    return CommonTransactionStatus.A;
                case XMLTransactionState.Rejected:
                    return CommonTransactionStatus.D;
                case XMLTransactionState.Done:
                    return CommonTransactionStatus.R;
                default:
                    throw new NotSupportedException(state.ToString());
            }
        }

        public static CommonTransactionStatus ToCommonTransactionState(this CSVTransactionState state)
        {
            switch (state)
            {
                case CSVTransactionState.Approved:
                    return CommonTransactionStatus.A;
                case CSVTransactionState.Failed:
                    return CommonTransactionStatus.D;
                case CSVTransactionState.Finished:
                    return CommonTransactionStatus.R;
                default:
                    throw new NotSupportedException(state.ToString());
            }
        }
    }
}
