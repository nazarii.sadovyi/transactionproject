﻿using System;
using TransactionApp.Common.Enums;

namespace TransactionApp.Common.Entities
{
    public class TransactionFilterParameter
    {
        public string Currency { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public CommonTransactionStatus? Status { get; set; }
    }
}
