﻿using System.Collections.Generic;
using TransactionApp.Common.Domain;

namespace TransactionApp.Common.Entities
{
    public class TransactionParseResult
    {
        public ICollection<Transaction> CorrectRecords { get; set; }

        /// <summary>
        /// Key is number of line and value is error message
        /// </summary>
        public ICollection<KeyValuePair<int, string>> IncorrectRecords { get; set; }
        public ICollection<string> IncorrectIds { get; set; }

        public TransactionParseResult()
        {
            CorrectRecords = new List<Transaction>();
            IncorrectIds = new List<string>();
            IncorrectRecords = new List<KeyValuePair<int, string>>();
        }
    }
}
