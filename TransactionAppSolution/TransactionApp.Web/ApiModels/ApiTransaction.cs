﻿using TransactionApp.Common.Domain;

namespace TransactionApp.Web.ApiModels
{
    public class ApiTransaction
    {
        public string Id { get; set; }
        public string Payment { get; set; }
        public string Status { get; set; }

        public ApiTransaction(Transaction domainTransaction)
        {
            Id = domainTransaction.Id;
            Payment = $"{domainTransaction.Amount} {domainTransaction.CurrencyCode}";
            Status = domainTransaction.State.ToString();
        }
    }
}
