﻿using System;
using TransactionApp.Common.Enums;

namespace TransactionApp.Web.ApiModels
{
    public class FilterParameters
    {
        public string Currency { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public CommonTransactionStatus? Status { get; set; }
    }
}
