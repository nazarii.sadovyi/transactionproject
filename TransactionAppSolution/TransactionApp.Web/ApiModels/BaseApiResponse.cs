﻿using System.Net;

namespace TransactionApp.Web.ApiModels
{
    public class BaseApiResponse
    {
        public bool IsError { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public object Data { get; set; }

        public BaseApiResponse()
        {
            StatusCode = HttpStatusCode.OK;
        }
    }
}
