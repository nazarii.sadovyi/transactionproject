﻿using System.Collections.Generic;
using System.Linq;
using TransactionApp.Common.Domain;

namespace TransactionApp.Web.ApiModels
{
    public class PostTransactionResponse
    {
        public IEnumerable<ApiTransaction> Imported { get; set; }
        public IEnumerable<string> IncorrectRecords { get; set; }
        public IEnumerable<string> IncorrectIds { get; set; }
        public PostTransactionResponse(IEnumerable<string> incorrectRecords, 
            IEnumerable<Transaction> imported, IEnumerable<string> incorrectIds)
        {
            IncorrectRecords = incorrectRecords;
            Imported = imported.Select(x => new ApiTransaction(x));
            IncorrectIds = incorrectIds;
        }
    }
}
