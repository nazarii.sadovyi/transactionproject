﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TransactionApp.Common.Entities;
using TransactionApp.Common.Enums;
using TransactionApp.Core.Services;

namespace TransactionApp.Web.Controllers
{
    public class HomeController : Controller
    {
        private ITransactionService _transactionService;
        public HomeController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        public IActionResult Index()
        {
            var res = _transactionService.Filter(new TransactionFilterParameter()
            {
                Currency = "USD",
                Status = CommonTransactionStatus.D
            });

            return View();
        }

        public IActionResult Upload()
        {
            return View();
        }
    }
}
