﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Net;
using TransactionApp.Common.Entities;
using TransactionApp.Common.Exceptions;
using TransactionApp.Core.Services;
using TransactionApp.Web.ApiModels;

namespace TransactionApp.Web.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly string _tempDirectotyPath;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
            _tempDirectotyPath = Path.GetTempPath();
        }

        [HttpGet]
        public IActionResult Get([FromQuery]TransactionFilterParameter transactionFilter)
        {
            var apiTransaction = _transactionService
                .Filter(transactionFilter)
                .Select(x => new ApiTransaction(x));

            return new JsonResult(
                new BaseApiResponse()
                {
                    Data = apiTransaction
                }
            );
        }

        [HttpPost]
        public IActionResult Post([FromForm]IFormFile file)
        {
            var response = new BaseApiResponse();

            var tepmFileName = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
            var tempFilePath = Path.Combine(_tempDirectotyPath, tepmFileName); 

            using (var fileStream = new FileStream(tempFilePath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            try
            {
                var result = _transactionService.ImportTransactionFromFile(tempFilePath);
                var postTransactionResponse = new PostTransactionResponse(
                        result.IncorrectRecords.Select(x => $"{x.Key}: {x.Value}"),
                        result.CorrectRecords,
                        result.IncorrectIds
                    );
                response.Data = postTransactionResponse;
            }
            catch (NotSuportedFileExtension e)
            {
                response.IsError = true;
                response.Data = e.Message;
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.Data = e.Message;
                response.StatusCode = HttpStatusCode.InternalServerError;
            }

            System.IO.File.Delete(tempFilePath);

            return new JsonResult(response);
        }
    }
}